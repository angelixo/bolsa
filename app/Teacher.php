<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;


class Teacher extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'apellidos', 'nrp_expediente', 'is_admin'
    ];

    protected $dates = ['deleted_at'];


    /** RELACIONES **/ //Aquí las relaciones


    //Relacion con user
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    //Relacion con TeacherValidations
    public function validations()
    {
        return $this->hasMany('App\TeacherValidation')->orderBy("created_at", "desc");
    }

    //Relacion con OfferSelections
    public function selections()
    {
        return $this->hasMany('App\OfferSelection')->orderBy("created_at", "desc");
    }

    //Relacion con ciclos
    public function cicles()
    {
        return $this->belongsToMany('App\Cicle', 'cicle_teachers', 'teacher_id', 'cicle_id')->withPivot('cicle_id', 'promocion')->withTimestamps();
    }

    public function getOffersAttribute()
    {
        $model = [];
        foreach($this->selections as $selection){
            $model[$selection->offer->id] = $selection->offer;
        }
        return $model;
    }






    /** SETTERS **/ //Aquí los setters

    public function setIsAdminAttribute($value)
    {
        ($value) ? $is_admin = 1 : $is_admin = 0;
        $this->attributes['is_admin'] = $is_admin;
    }







    /** SCOPES **/ //Aquí los scopes

    //scope para los apellidos del profesor
    public function scopeApellidos($query, $apellidos)
    {

    }

    //scope para el nrp del profesor
    public function scopeNrp($query, $nrp)
    {

    }

    //scope para isAdmin del profesor
    public function scopeIsAdmin($query, $isadmin)
    {
        if (trim($isadmin) != '') {
            $query->where('is_admin', '=', $isadmin);
        }

    }

    public function scopeNoActive($query)
    {
        $query->with("user")->whereHas('user', function ($q) {
            //Reutilizo el scopeActive del modelo User.
            $q->Active(0);
        });
    }

    //scope para isActive de Usuario (vinculado con profesor)
    public function scopeActive($query)
    {
        $query->with("user")->whereHas('user', function ($q) {
            //Reutilizo el scopeActive del modelo User.
            $q->Active(1);
        });
    }

    // Refactorizar. Incluir Scopes de la clase.
    // scope para la búsqueda (por métodos de búsqueda)
    public function scopeSearch($query, Request $request)
    {
        //Analizo si tiene método de búsqueda y si tiene el campo search
        if ($request->has("method") && $request->has("search")) {

            if (trim($request->get("search")) != '') {

                //Asignación del término de búsqueda. El use no permite métodos.
                $search = $request->get("search");


                //Hago un switch con los métodos de búsqueda (Por nombre, apellidos, nrp, etc) y ejecuto el scope con el campo búsqueda
                switch ($request->get("method")) {

                    case "nombre":
                        //Refactorizar
                        $query->with("user")->whereHas('user', function ($q) use ($search) {
                            $q->Name($search);

                        });
                        break;

                    case "apellidos":
                        //usar this->scopeApellidos
                        $query->where('apellidos', 'LIKE', "%" . $search . "%");

                        break;

                    case "nrp":
                        //usar this->scopeNrp
                        $query->where('nrp_expediente', '=', "$search");

                        break;

                    case "telefono":
                        //Refactorizar
                        $query->with("user")->whereHas('user', function ($q) use ($search) {
                            $q->Telefono($search);
                        });

                        break;

                    case "email":
                        //Refactorizar
                        $query->with("user")->whereHas('user', function ($q) use ($search) {
                            $q->Email($search);
                        });

                        break;

                }

            }


        }
    }

}
