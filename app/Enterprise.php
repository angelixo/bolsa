<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;


class Enterprise extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'descripcion', 'sociedad', 'cif','fax','fecha_fundacion','web',
        'pais','ciudad','score','min_empleados','max_empleados'
    ];

    protected $dates=['deleted_at'];


    /** RELACIONES **/ //Aquí las relaciones

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function offers()
    {
        return $this->hasMany('App\Offer');
    }


    /** GETTERS **/ //Aquí los GETTERS
    public function getNameAttribute()
    {
        return $this->user->name;
    }


    /** SETTERS **/ //Aquí los setters





<<<<<<< HEAD

=======
>>>>>>> master

    /** SCOPES **/ //Aquí los scopes
    public function scopeDescripcion($query, Request $request){


        $descripcion="%".$request->datos."%";

        $query->where('descripcion', '$descripcion');

    }

    //scope para isActive de Usuario (vinculado con profesor)
    public function scopeActive($query)
    {
        $query->with("user")->whereHas('user', function($q)
        {
            //Reutilizo el scopeActive del modelo User.
            $q->Active(1);
        });
    }
    public function scopeNoActive($query)
    {
        $query->with("user")->whereHas('user', function($q)
        {
            //Reutilizo el scopeActive del modelo User.
            $q->Active(0);
        });
    }

    // Refactorizar. Incluir Scopes de la clase.
    // scope para la búsqueda (por métodos de búsqueda)
    public function scopeSearch($query,Request $request)
    {
        //Analizo si tiene método de búsqueda y si tiene el campo search
        if($request->has("method") && $request->has("search")){

            if(trim($request->get("search")) != ''){

                //Asignación del término de búsqueda. El use no permite métodos.
                $search = $request->get("search");


                //Hago un switch con los métodos de búsqueda (Por nombre, apellidos, nrp, etc) y ejecuto el scope con el campo búsqueda
                switch ($request->get("method")){

                    case "nombre":
                        //Refactorizar
                        $query->with("user")->whereHas('user', function($q) use ($search)
                        {
                            $q->Name($search);

                        });
                        break;

                    case "web":
                        //usar this->scopeApellidos
                        $query->where('web','LIKE',"%".$search."%");

                        break;

                    case "cif":
                        //usar this->scopeNrp
                        $query->where('cif','=',"$search");

                        break;

                    case "descripcion":
                        //usar this->scopeNrp
                        $query->where('descripcion','LIKE',"%$search%");

                        break;

                    case "sociedad":
                        //usar this->scopeNrp
                        $query->where('sociedad','LIKE',"%$search%");

                        break;

                    case "telefono":
                        //Refactorizar
                        $query->with("user")->whereHas('user', function($q) use ($search)
                        {
                            $q->Telefono($search);
                        });

                        break;

                    case "email":
                        //Refactorizar
                        $query->with("user")->whereHas('user', function($q) use ($search)
                        {
                            $q->Email($search);
                        });

                        break;

                }

            }


        }
    }
}
