<?php

namespace App\Http\Controllers;

/** REQUEST */
use Illuminate\Http\Request;
use App\Http\Requests\StudentStoreRequest;
use App\Http\Requests\StudentUpdateRequest;

/** MODELOS */
use App\User;
use App\Student;
use App\Cicle;

/** MAILS */
use App\Mail\newRegisteredUser;
use Illuminate\Support\Facades\Mail;

/** OTROS */
use Illuminate\Support\Facades\Session;


class StudentController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        // Refactorizar. Usar scopes creados en modelo User.
        // Crear los Scopes necesarios (los de todos los campos) en el modelo Student para evitar repetir código en el futuro.

        $students = Student::whereHas('user', function ($query) use ($request) {
            $query->where('name', 'like', "%" . $request->datos . "%")
                ->orWhere('email', 'like', "%" . $request->datos . "%");
        })->datos($request->datos)->Active()->paginate(100);
        return view("student.index", compact("students"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $cicles = get_model_selectable_by_name(Cicle::all());
        return view("student.create", compact("cicles"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StudentStoreRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentStoreRequest $request) {
        //Crear alumno 1º

        $user = new User($request->only(["email","password","name"]));
        $user->phone = $request->get("phone");
        $user->is_active = 0;
        $user->save();

        $student = $user->student();

        $opts = [
            "apellidos" => $request->get("apellidos"),
            "nre" => $request->get("nre"),
            "vehiculo" => $request->get("vehiculo"),
            "domicilio" => $request->get("domicilio"),
            "status" => $request->get("status"),
            "edad" => $request->get("edad")
        ];
        if($request->get("vehiculo") == null) { //Si la contraseña es nula no se cambia
            $opts["vehiculo"] = 0;
        }

        $student->create($opts);

        Session::flash('message', 'Estudiante agregado correctamente');

        Mail::to($user->email)->send(new newRegisteredUser($user)); 

//        $respuesta = true;
        return redirect()->route("student.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student) {

        return view("student.show", compact("student"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student) {
        //Paso el id a sesion para que la validación excluya este id cuando se vaya a editar
        Session::flash('user_id', $student->user->id);
        //trampa mortal
        //mejorar para que en la vista coja la relacion
        $student->email = $student->user->email;
        $student->name = $student->user->name;
        $student->phone = $student->user->phone;

        $cicles = get_model_selectable_by_name(Cicle::all());

        return view("student.edit", compact("student","cicles"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(StudentUpdateRequest $request, Student $student) {

        $user = User::findOrFail($student->user_id);
        $student = Student::findOrFail($student->id);

        $optsUser = [
            "email" => $request->get("email"),
            "name" => $request->get("name"),
        ];
        $user->phone = $request->get("phone");

        $optsStudent = [
            "apellidos" => $request->get("apellidos"),
            "nre" => $request->get("nre"),
            "vehiculo" => $request->get("vehiculo"),
            "domicilio" => $request->get("domicilio"),
            "edad" => $request->get("edad"),
        ];
        $student->status = $request->get("status");

        if($request->get("password") != null) { //Si la contraseña es nula no se cambia
            $optsUser["password"] = $request->get("password");
        }
        if($request->get("vehiculo") == null) { //Si la contraseña es nula no se cambia
            $optsStudent["vehiculo"] = 0;
        }

        $user->fill($optsUser);
        $student->fill($optsStudent);

        $user->save();
        $student->save();

        Session::flash('message', 'Estudiante editado correctamente');
//        $respuesta = true;
        return redirect()->route("student.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student) {

        $user = User::findOrFail($student->user_id);

        $student->delete();
        $user->delete();

        return redirect()->route("student.index");
    }

    public function inactive()
    {
        $students = Student::NoActive()->get();
        return view("student.inactive", compact("students"));
    }

    public function newCicle(Request $request, Student $student)
    {
        $student->cicles()->attach($request->get("cicle_id"), ['promocion' => $request->get("promocion"),"finalizado" => ($request->get("finalizado"))?"1":"0"]);
        return redirect()->back();
    }

    public function delCicle( Student $student,Cicle $cicle)
    {
        $student->cicles()->detach($cicle->id);
        return redirect()->back();
    }
 
}