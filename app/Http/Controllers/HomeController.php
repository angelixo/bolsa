<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\TeacherValidation;
use \App\Student;
use \App\Enterprise;
use Faker\Factory as Faker;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        switch (auth()->user()->rol) {
            case 'is_admin':
                return $this->indexAdmin();
                break;

            case 'is_teacher':
                return $this->indexTeacher();
                break;

            case 'is_student':
                return $this->indexStudent();
                break;

            case 'is_enterprise':
                return $this->indexEnterprise();
                break;
            
            default:
                return redirect()->route("index");
                break;
        }
        
    }

    public function indexAdmin(){
        
        $allValidations = TeacherValidation::orderBy("created_at","desc")->take(7)->get();
        $currentUserValidations = auth()->user()->teacher->validations->take(7);
        return view("index.admin",compact("allValidations","currentUserValidations"));  
    }

    public function indexTeacher(){
        $faker = Faker::create("es_ES");
        $quote = $faker->randomElement(config("quotes"));
        $studentCount = Student::NoActive()->count();
        $enterpriseCount = Enterprise::NoActive()->count();
        return view("index.teacher",compact("studentCount","enterpriseCount","quote"));   
    }

    public function indexStudent(){
        return view("index.student");
    }

    public function indexEnterprise(){

        return view("index.enterprise");
    }
}
