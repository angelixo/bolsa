<?php

namespace App\Http\Controllers;

/** REQUEST */
use Illuminate\Http\Request;
use App\Http\Requests\ProfessionalFamilyStoreRequest;
use App\Http\Requests\ProfessionalFamilyUpdateRequest;

/** MODELOS */
use App\Family;

/** MAILS */

/** OTROS */
use Illuminate\Support\Facades\Session;

class FamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $profesionalfamilys = Family::Name($request)->orderBy("id","asc")->paginate(10);

        return view("family.index",compact("profesionalfamilys","request"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('family.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfessionalFamilyStoreRequest $request)
    {
        $family = new Family(["name" => $request->get("name")]);
        $family->save();
        Session::flash('message', 'Familia profesional agregada correctamente');
        return redirect()->route("family.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function show(Family $family)
    {
        //dd($family);
        return view("family.show",compact("family"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function edit(Family $family)
    {
        return view("family.edit",compact("family"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function update(ProfessionalFamilyUpdateRequest $request, Family $family)
    {
        //dd($family);
        $family->fill(["name" => $request->get("name")]);
        $family->save();
        Session::flash('message', 'Familia profesional editada correctamente');

        return redirect()->route("family.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function destroy(Family $family)
    {
        $family->delete();

        return redirect()->route("family.index");
    }
}
