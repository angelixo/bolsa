<?php

namespace App\Http\Controllers;

use App\Enterprise;
use App\Mail\OffertAssigned;
use App\Offer;
use App\OfferSelection;
use App\OfferSubscription;
use App\Student;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Family;
use Faker\Factory as Faker;
use Carbon\Carbon;


use Illuminate\Support\Facades\Mail;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $families = get_model_selectable_by_name(Family::all());
        $offers = Offer::search($request)->PorConfirmar()->orderBy("created_at","desc")->paginate(10);
        $faker = Faker::create("es_ES");
        $quote = $faker->randomElement(config("quotes"));

        return view('offers.index',compact('offers',"quote","families","request"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $families = get_model_selectable_by_name(Family::all());

        if (auth()->user()->rol == "is_admin"){
            $enterprises = get_model_selectable_by_name(Enterprise::all());
            $opts = compact("families","enterprises");
        }else{
            $opts = compact("families");
        }

        return view('offers.create', $opts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Crear OFERTA
        $offer = new Offer;
        //id del auth

        if (auth()->user()->rol == "is_admin"){
            $offer->enterprise_id = $request->get("enterprise_id");
        }else{
            $offer->enterprise_id = auth()->user()->enterprise->id;
        }


        $offer->requirements = $request->requirements;
        $offer->recommended = $request->recommended;
        $offer->description = $request->description;
        $offer->title = $request->title;
        $offer->work_day = $request->work_day;
        $offer->schedule = $request->schedule;
        $offer->contract = $request->contract;
        $offer->salary = $request->salary;
        $offer->status = "Pend_Validacion";
        $offer->student_number = $request->student_number;
        $offer->start_date = $request->start_date;
        $offer->end_date = $request->end_date;
        $offer->family_id = $request->family_id;

        $respuesta = $offer->save();

        //Crear y guardar los modelos de las relaciones
        return redirect()->route("offers.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Offer $offer
     * @return \Illuminate\Http\Response
     */
    public function show(Offer $offer)
    {
        return view('offers.show',compact('offer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Offer $offer
     * @return \Illuminate\Http\Response
     */
    public function edit(Offer $offer)
    {
        $families = get_model_selectable_by_name(Family::all());

        return view('offers.edit',compact('offer', "families"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Offer $offer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offer $offer)
    {

        $offer->fill($request->only(["requirements", "recommended", "title", "work_day", "schedule", "contract", "start_date", "description", "salary", "student_number", "family_id"]));
        $offer->status = "Pend_Validacion";
        $offer->save();

        return redirect()->route("offers.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Offer $offer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offer $offer)
    {
        $offer->delete();

        return redirect()->route("offers.index");
    }

    public function inactive()
    {
        $offers = Offer::NoActive()->get();
        return view("offers.inactive",compact("offers"));
    }

    public function activate(Offer $offer)
    {
        Carbon::setLocale('es');
        $aux = [];
        //Mete en un array todos los ids de los estudiantes que ya estan en la oferta
        foreach ($offer->selections as $selected) {
            $aux[] = $selected->student->id;
        }

        //Selecciona todos los alumnos que no estén en aux (en la oferta) [40,120,10]
        $students = Student::Active()->whereNotIn('id',$aux)->orderBy("apellidos")->get();
        $selected = $offer->selections;
        return view("offers.validate", compact("offer", "students","selected"));
    }

    public function assign(Request $request, Offer $offer)
    {


        $teacher = auth()->user()->teacher;
        if (count($request->get("students")) > 0) {
            foreach ($request->get("students") as $studentRequested) {
                //Encuentra el Alumno
                $student = Student::findOrFail($studentRequested);

                //Comprueba si ese alumno ya ha sido asignado a esa oferta
                $selections = OfferSelection::where("offer_id",$offer->id)->where("student_id",$student->id)->get();
                if ($selections->isEmpty()){
                    $selections_opt = ["student_id" => $student->id, "offer_id" => $offer->id];
                    $teacher->selections()->create($selections_opt)->save();



                    Mail::to($student->user->email)->send(new OffertAssigned($student,$offer)); //Mail


                }
            }



        }


        $offer->status = "Pend_Confirmacion";
        $offer->save();
        
        return redirect()->route("offers.activate",$offer->id);
    }

    public function selectedDelete(OfferSelection $selected)
    {
        $id = $selected->offer->id;
        $selected->delete();

        return redirect()->route("offers.activate",$id);
    }

    public function subscribe(Offer $offer)
    {
        if (!$offer->subscribed_by_auth){
            auth()->user()->student->subscriptions()->create(["offer_id"=>$offer->id])->save();
        }

        return redirect()->back();
    }

    public function unsubscribe(Offer $offer)
    {
        if ($offer->subscribed_by_auth){
            $sub = OfferSubscription::where("offer_id",$offer->id)->where("student_id",auth()->user()->student->id)->first();
            $sub->delete();
        }

        return redirect()->back();
    }


    public function proffer(OfferSelection $selection)
    {

        $offer = $selection->offer;
        return view("offers.proffer",compact("offer","selection"));
    }

    public function answer(Request $request,OfferSelection $selection)
    {
        $answer = ($request->get("answer") == "SI")?"1":"0";
        $selection->fill(["answer" => $answer])->save();

        if ($answer == "1")
            return redirect()->route("offers.show",$selection->offer->id);
        else
            return redirect()->route("home");

    }
}
