<?php

namespace App\Http\Controllers;

/** REQUEST */
use Illuminate\Http\Request;
use App\Http\Requests\EnterpriseStoreRequest;
use App\Http\Requests\EnterpriseUpdateRequest;

<<<<<<< HEAD
class EnterpriseController extends Controller
{
    public function index(){

        $listado=Enterprise::paginate();

        return view('empresa.lista',compact('listado'));

    }
=======
/** MODELOS */
use App\Offer;
use App\Enterprise;
use App\User;
>>>>>>> master

/** MAILS */
use App\Mail\newRegisteredEnterprise;
use Illuminate\Support\Facades\Mail;

<<<<<<< HEAD

    public function create(){

        return view('empresa.formularioAgregar');
    }

    public function store($request){

       //Espacio para crear al usuario

        $enterprise = new Enterprise();

            $enterprise->id_user= '1';
            $enterprise->descripcion = $request->descripcion;
            $enterprise->sociedad = $request->sociedad;
            $enterprise->cif = $request->cif;
            $enterprise->fax = $request->fax;
            $enterprise->fecha_fundacion = $request->fecha_fundacion;
            $enterprise->web = $request->web;
            $enterprise->pais = $request->pais;
            $enterprise->ciudad = $request->ciudad;
            $enterprise->min_empleados = $request->min_empleados;
            $enterprise->max_empleados = $request->max_empleados;

        if($enterprise->save()){
=======
/** OTROS */
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Session;

class EnterpriseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $empresas= Enterprise::Search($request)->Active()->paginate(10);

        return view('enterprise.index', compact('empresas','request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('enterprise.create');
    }
>>>>>>> master

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EnterpriseStoreRequest $request)
    {
        $user= new User($request->only(["name","email","password"]));
        $user->phone = $request->get("phone");
        $user->is_active = false;
        $user->save();

        $user->enterprise()->create($request->only(["descripcion","sociedad","cif","fax","fecha_fundacion","web","pais","ciudad","min_empleados","max_empleados"]));

        if($user->enterprise->save()){
            Session::flash('message', 'Empresa agregada correctamente');
            /*
             * Email, sólo nesitamos darle el email  + datos del usuario.
             */
            Mail::to($user->email)->send(new newRegisteredEnterprise($user));
            return redirect()->route("enterprise.index");
        }else {
            return "ERROR";
        }
    }

<<<<<<< HEAD
    public function show($id){

        $empresa = new Empresa();
        $empresa = $empresa->findOrFail($id);


        return view('empresa.modificar', compact('empresa'));

    }


    public function update($request){


        dd($request);exit();
        $enterprise = Enterprise::find($request->id);
=======
    /**
     * Display the specified resource.
     *
     * @param  \App\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function show(Enterprise $enterprise)
    {
        $offers = $enterprise->offers;
        return view('enterprise.show',compact('enterprise',"offers"));
    }
>>>>>>> master

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function edit(Enterprise $enterprise)
    {
        Session::flash('user_id', $enterprise->user->id);
        $user = $enterprise->user->toArray();
        unset($user["id"]);
        $user = array_merge($enterprise->toArray(),$user);
        $empresa = new Collection($user);

        return view('enterprise.edit',compact('empresa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function update(EnterpriseUpdateRequest $request, Enterprise $enterprise)
    {
        $enterprise->user->fill($request->only(["name","email"]));
        $enterprise->user->phone = $request->get("phone");
        $enterprise->user->save();
        $enterprise->fill($request->only(["descripcion","sociedad","cif","fax","fecha_fundacion","web","pais","ciudad","min_empleados","max_empleados"]));

        if($enterprise->save()){
            Session::flash('message', 'Empresa editada correctamente');
            return redirect()->route("enterprise.index");

<<<<<<< HEAD
            return ;
=======
>>>>>>> master
        }else {
            return "Error en la Edicion";
        }
    }

<<<<<<< HEAD
    public function destroy($request){


=======
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function destroy(Enterprise $enterprise)
    {
        $enterprise->user()->delete();
        $enterprise->delete();

        return redirect()->route("enterprise.index");
    }
>>>>>>> master

    public function inactive()
    {
        $enterprises = Enterprise::NoActive()->get();
        return view("enterprise.inactive", compact("enterprises"));
    }

}
