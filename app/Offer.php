<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Carbon\Carbon;

class Offer extends Model
{
    protected $fillable =[
        'requirements', 'recommended', 'title', 'work_day', 'schedule', 'contract', 'start_date', "description", "salary", "student_number", "family_id",
    ];

    /** RELACIONES **/ //Aquí las relaciones
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function enterprise()
    {
        return $this->belongsTo('App\Enterprise');
    }
    public function family() {
        return $this->belongsTo('App\Family');
    }

    public function selections()
    {
        return $this->hasMany('App\OfferSelection');
    }

    public function selectionsPositive()
    {
        return $this->hasMany('App\OfferSelection')->where("answer","1");
    }
    public function selectionsPending()
    {
        return $this->hasMany('App\OfferSelection')->where("answer","2");
    }

    public function selectionsNegative()
    {
        return $this->hasMany('App\OfferSelection')->where("answer","0");
    }

    public function subscriptions()
    {
        return $this->hasMany('App\OfferSubscription');
    }





    /** GETTERS **/ //Aquí los getters
    public function getFollowedByAuthAttribute()
    {

        if (!$this->selected_by_auth | !$this->subscribed_by_auth) {
            return false;
        } else {
            return true;
        }

    }

    public function getSelectedByAuthAttribute()
    {
        if (auth()->user()->rol != "is_student") {
            return false;
        } else {
            $selected = OfferSelection::where("offer_id", $this->id)->where("student_id", auth()->user()->student->id)->get();
            if ($selected->isEmpty()) {
                return false;
            } else {
                return true;
            }
        }
    }

    public function getConfirmedSelectionByAuthAttribute()
    {
        if (auth()->user()->rol != "is_student") {
            return false;
        } else {
            $selected = OfferSelection::where("offer_id", $this->id)->where("student_id", auth()->user()->student->id)->where("answer","1")->first();
            if (empty($selected)) {
                return false;
            } else {
                return $selected->id;
            }
        }
    }

    public function getPendingSelectionByAuthAttribute()
    {
        if (auth()->user()->rol != "is_student") {
            return false;
        } else {
            $selected = OfferSelection::where("offer_id", $this->id)->where("student_id", auth()->user()->student->id)->where("answer","2")->first();
            if (empty($selected)) {
                return false;
            } else {
                return $selected->id;
            }
        }
    }

    public function getDeniedSelectionByAuthAttribute()
    {
        if (auth()->user()->rol != "is_student") {
            return false;
        } else {
            $selected = OfferSelection::where("offer_id", $this->id)->where("student_id", auth()->user()->student->id)->where("answer","0")->first();
            if (empty($selected)) {
                return false;
            } else {
                return $selected->id;
            }
        }
    }

    public function getSubscribedByAuthAttribute()
    {
        if (auth()->user()->rol != "is_student") {
            return false;
        } else {
            $subscribbed = OfferSubscription::where("offer_id", $this->id)->where("student_id", auth()->user()->student->id)->get();
            if ($subscribbed->isEmpty()) {
                return false;
            } else {
                return true;
            }
        }
    }



    /** SETTERS **/ //Aquí los setters


    /** SCOPES **/ //Aquí los scopes

    public function scopeFamilySearch($query, $family) {
        if(intval($family) > 0){
            return $query->where("family_id", "$family");
        }
        return $query;
    }

    public function scopeWordSearch($query, $search) {
        $query->orWhere('title', 'like', "%".$search."%")
              ->orWhere("description", "like", "%$search%")
              ->orWhere('work_day', 'like', "%".$search."%");
    }

    public function scopeDateSearch($query, $date_add) {

        if($date_add == null){  // Validación de "start_date"
            $filterDate = (Offer::select("start_date")->orderBy("start_date", "ASC")->first())->start_date;
        } else{
            $date = Carbon::now();
            $filterDate = $date->subDay(array_pop($date_add))->toDateString();
        }

        return $query->where("start_date", ">=", "$filterDate");

    }

    public function scopeContractSearch($query, $contrato) {
        if($contrato == null)   // Validación de "contact"
            $contrato = config("arrayData")["offerContract"];

        return $query->whereIn("contract", $contrato);
    }

    public function scopeWorkDaySearch($query, $work_day) {
        if($work_day == null)   // Validación de "work_day"
            $work_day = config("arrayData")["offerWork_Day"];

        return $query->whereIn("work_day", $work_day);
    }

    public function scopeSalarySearch($query, $salario) {
        
        if($salario == null){    // Validación de $salario
            $maxSalary = Offer::max("salary");
            $salario[0] = 0;
            $salario[1] = $maxSalary;
        } else
            $salario =  explode(";", $salario);

        return $query->where("salary",">=", $salario[0])
                     ->where("salary","<=", $salario[1]);
    }

    public function scopeSearch($query, Request $request) {
        return $query->wordSearch($request->get("search"))->familySearch($request->get("family"))->dateSearch($request->get("date_add"))->contractSearch($request->get("contrato"))->workDaySearch($request->get("work_day"))->SalarySearch($request->get("salario"));
    }

    public function scopeNoActive($query)
    {
        $query->where('status', '=', "Pend_Validacion");
    }


    public function scopePorConfirmar($query)
    {
        $query->where('status', '=', "Pend_Confirmacion");
    }


}
