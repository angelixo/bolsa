<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Cicle extends Model
{
    protected $fillable = [
        'name', "plan", "tipo", "family_id", "created_at"
    ];

    /** RELACIONES **/ //Aquí las relaciones
    public function family()
    {
        return $this->belongsTo('App\Family');
    }

    //Relacion con teachers
    public function teachers()
    {
        return $this->belongsToMany('App\Teacher', 'cicle_teachers', 'cicle_id', 'teacher_id')->withTimestamps();
    }

    //Relacion con student
    public function students()
    {
        return $this->belongsToMany('App\Student', 'student_courses', 'cicle_id', 'student_id');
    }



    /** GETTERS **/ //Aquí los getters


    /** SETTERS **/ //Aquí los setters


    /** SCOPES **/ //Aquí los scopes
    public function scopeName($query, Request $request)
    {
        $name = $request->get("name");
        if (trim($name) != '') {
            $query->where('name', 'LIKE', "%$name%");
        }
    }
}
