@extends('layouts.mail')

<!--We're in a table . Estamos en una tabla -->
@section('content')



                                <tr>
                                    <td class="content-block" style="text-align:center;">


                                        <h2>Enhorabuena {{$student->user->name}}, te han recomendado un empleo.</h2>
                                    </td>
                                </tr>

                                <tr>
                                    <td><h3>Nueva oferta, numero: {{ $offer->id }}</h3></td>
                                </tr>

                                <tr>
                                    <td>Titulo </td>
                                    <td>{{ $offer->title }}</td>

                                </tr>


                                <tr>
                                    <td>Descripción </td>
                                    <td>{{$offer->description}}</td>

                                </tr>

                                <tr>
                                    <td>Requisitos </td>
                                    <td>{{$offer->requirements}}</td>

                                </tr>

                                <tr>
                                    <td>Horario </td>
                                    <td>{{$offer->work_day}}, {{$offer->schedule}}</td>

                                </tr>

                                <tr>
                                    <td>Salario </td>
                                    <td>{{$offer->salary}}€</td>

                                </tr>

                                <tr>
                                    <td>Contacto </td>
                                    <td>{{$offer->contract}}</td>

                                </tr>




                                <tr>

                                    <td class="aligncenter"><a href="#"> <button>Consultar mis ofertas</button></a></td>
                                </tr>

    @endsection