<table class="table table-hover" id="table-data"> 
    <thead> 
        <tr> 
            <th>#</th> 
            <th>Alumno</th> 
            <th>NRE</th> 
            <th>Email</th> 
            <th>Estado</th> 
            <th>Teléfono</th> 
            <th></th> 
            <th></th> 
        </tr> 
    </thead> 
    @foreach($students as $student) 
        <tr> 
            <td>{{ $student->user_id }}</td> 
            <td><a href="{{route("user.profile",$student->user->id)}}">{{ $student->user->name }} {{ $student->apellidos }}</a></td>
            <td>{{ $student->nre }}</td> 
            <td>{{ $student->user->email }}</td> 
            <td>{{ $student->status }}</td> 
            <td>{{ $student->user->phone }}</td> 
            <td>
                <a href="{{route("student.edit",$student->id)}}"><button href="#" class="btn btn-link">Editar</button></a>
            </td> 
            <td>
                {!! Form::open(['method' => 'DELETE', 'route' => ['student.destroy', $student->id]]) !!}
                    <button href="#" class="btn btn-link" type="submit">Eliminar</button>
                {!! Form::close() !!}
            </td> 
             
        </tr> 
 
    @endforeach 
</table>