@extends('layouts.layout')

@section('content')

	<div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">

                        <h2 align="center">Editar</h2>
                        <h4 align="center">{{$student["name"]}}</h4>
                    </div>
                    <div class="panel-body">

					    {{ Form::model($student, ['route' => ['student.update',$student],'method' => 'PUT']) }}
					    @include('student.partials.fields')
					    {{ Form::submit("Editar",['class' => 'btn btn-warning'])}}
					   
					    {{ Form::close() }}
    
					</div>
                </div>
            </div>
        </div>
    </div>

@endsection