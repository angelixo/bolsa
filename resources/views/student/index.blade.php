@extends('layouts.layout')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{route("student.create")}}" class="btn btn-info pull-left">Nuevo Alumno</a>
                        <form class="form form-inline col-md-offset-8">
                            <!-- Input con el término de la búsqueda -->
                            <input type="text" class="form-control" id="inlineFormInput" name="datos" placeholder="Ej: cmabris@gmail.com">
                            <button type="submit" class="btn btn-primary">Buscar</button>
                        </form>
                    </div>
                    <div class="panel-body">
                        <article class="container">

                            @include('student.partials.table')
                            {{$students->appends(Request::except('page'))->render()}}
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection