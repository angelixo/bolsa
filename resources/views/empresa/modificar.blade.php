
@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">Modificar Empresa</div>
                    <div class="panel-body">

                        {!! Form::model($empresa, ['url' => ['/empresa/editar', $empresa->id], 'method' => 'put']) !!}

                        @include("empresa/partials/camposFormularioEditar")

                        <div class="form-group">
                            {{Form::submit('Modificar Empresa', ['class' => 'btn btn-success pull-right'])}}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection