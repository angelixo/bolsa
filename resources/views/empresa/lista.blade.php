@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    {{ Form::open(['url' => '/empresa/buscar', 'method' => 'GET', 'class' => 'navbar-form navbar-left pull-right', 'role' => 'search']) }}
                    <div class="form-group input-group-sm">

                        {{ Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Buscar por nombre']) }}

                    </div>

                    <button type="submit" class="btn btn-info btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>

                    {{ Form::close() }}
                    <div class="panel-heading">Modificar y/o Borrar Empresas


                    </div>
                    <div class="panel-body">
                        <p>Mostrando página {{ $listado->currentPage() }} de {{ $listado->lastPage() }} <span class="pull-right">Total empresas: {{ $listado->total() }}</span></p>
                        <table class="table table-condensed">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Direccion</th>
                                <th>Telefono</th>
                                <th>Web</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($listado as $empresa)

                                <tr>

                                    <td>{{$empresa->id}}</td>
                                    <td>{{ $empresa->nombre }}</td>
                                    <td>{{ $empresa->direccion }}</td>
                                    <td>{{ $empresa->telefono }}</td>
                                    <td>{{ $empresa->web }}</td>
                                    <td>
                                        <a href="show/{{ $empresa->id }}" class="btn btn-info"><i class="fa fa-edit" aria-hidden="true">Editar</i>
                                            <a href="borrar/{{ $empresa->id }}" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true">Borrar</i>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $listado->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

