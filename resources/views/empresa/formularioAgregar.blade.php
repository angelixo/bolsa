@extends('layouts.app')


@section('content')

    <div class="container col-md-4 col-md-offset-4">

        @if (\Session::has('mensaje'))
            <div class="alert alert-success">
                <strong>{{ \Session::get('mensaje') }}</strong>
            </div>
        @endif

        <div class="panel panel-primary">
            <div class="panel-heading">Agregar Empresas</div>
            <div class="panel-body">

                {!! Form::open(['route' => ['enterprise.store'], 'method' => 'POST']) !!}

                @include("empresa/partials/camposFormularioAgregar")

                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection