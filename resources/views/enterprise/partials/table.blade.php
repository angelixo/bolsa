<table class="table table-hover ">
    <thead>
        <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Sociedad</th>
            <th>fecha_fundacion</th>
            <th>web</th>
            <th>Empleados</th>
            <th></th>
        </tr>
    </thead>
    @foreach($empresas as $empresa)
        <tr>
            <th>{{ $empresa->id }}</th>
            <th><a href="{{route("user.profile",$empresa->user->id)}}">{{ $empresa->user->name }}</a> </th>
            <th>{{$empresa->sociedad}}</th>
            <th>{{$empresa->fecha_fundacion}}</th>
            <th>{{$empresa->web}}</th>
            <th>{{$empresa->min_empleados}} - {{$empresa->max_empleados}}</th>
            <td>
                <a href="{{route('enterprise.edit',$empresa->id)}}"><button href="#" class="btn btn-link">Editar</button></a>
            </td>
            <td>
                {{ Form::open(['route' => ['enterprise.destroy',$empresa->id],'method' => 'DELETE']) }}
                <button href="#" class="btn btn-link" type="submit">Eliminar</button>
                {!! Form::close() !!}
            </td>
        </tr>

    @endforeach
</table>