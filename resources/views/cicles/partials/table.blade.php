<table class="table table-hover ">
    <thead>
        <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Tipo</th>
            <th>Plan</th>
            <th>Created</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    @foreach($formativeCicles as $formativeCicle)
        <tr>
            <td>
                {{$formativeCicle->id}}
            </td>
            <td>
                <a href="{{route("cicle.show",$formativeCicle->id)}}">{{$formativeCicle->name}}</a>
            </td>
            <td>
                <a href="{{route("cicle.show",$formativeCicle->id)}}">{{$formativeCicle->tipo}}</a>
            </td>
            <td>
                <a href="{{route("cicle.show",$formativeCicle->id)}}">{{$formativeCicle->plan}}</a>
            </td>
            <td>
                {{$formativeCicle->created_at->diffForHumans()}}
            </td>
            <td>
                <a href="{{route("cicle.edit",$formativeCicle->id)}}">Editar</a>
            </td>
            <td>
                {!! Form::open(['method' => 'DELETE', 'route' => ['cicle.destroy', $formativeCicle->id]]) !!}
                        <button href="#" class="btn btn-link" type="submit">Eliminar</button>
                {!! Form::close() !!}

            </td>
        </tr>
        @endforeach
</table>