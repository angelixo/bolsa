@extends('layouts.layout')

@section('content')
    <div class="container">
        <hr>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <a href="{{route("family.create")}}" class="btn btn-info pull-left">Nueva Familia</a>
                <form class="form-inline col-md-offset-8">

                    <!-- Input con el término de la búsqueda -->
                    <input type="text" class="form-control" id="inlineFormInput" name="name"
                           placeholder="Ej: Informática">
                    <button type="submit" class="btn btn-primary">Buscar</button>
                </form>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @include('family.partials.table')

                {{$profesionalfamilys->appends($request->only(['name']))->render()}}
            </div>
        </div>
    </div>
    </div>

@endsection