<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @if(!$enterprises->isEmpty())
            {!! Form::open(['method' => 'post', 'route' => ['user.toggle.selected']]) !!}
            @endif
            <article class="container">
                @if(!$enterprises->isEmpty())
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                            <input type="checkbox" class="check-padre" id="enterprise">Marcar Todos
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sm-offset-7">
                            <button class="btn btn-success" type="submit">Validar Seleccionados</button>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
                @endif
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                    @if(!$enterprises->isEmpty())
                    @foreach($enterprises as $enterprise)
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <div class="checkbox">
                                  <h4><label><input type="checkbox" name="selected[]" id="enterprise" value="{{$enterprise->user->id}}">{{ $enterprise->user->name }} <small>{{ $enterprise->sociedad }}</small></label></h4>
                                </div>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Buscar en Google"><a href="https://www.google.es/search?q={{str_slug($enterprise->user->name ." ".$enterprise->sociedad,"+")}}" target="_blank"><img class="img-responsive img-circle" style="width: 20px; height: auto" src="https://cdn4.iconfinder.com/data/icons/new-google-logo-2015/400/new-google-favicon-128.png"></a></li>
                                   <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                        <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                             <li><a href="https://www.google.es/search?q={{str_slug($enterprise->user->name ." ".$enterprise->sociedad,"+")}}">Buscar en Google</a></li>
                                             <li><a target="_blank" href="{{$enterprise->web}}">Visitar web</a></li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>

                            <div class="x_content">
                                <div class="slimScroll">
                                   <table class="table table-striped">
                                        <tr>
                                            <td>
                                                Email:
                                            </td>
                                            <td>
                                                {{$enterprise->user->email}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Sociedad:
                                            </td>
                                            <td>
                                                {{$enterprise->sociedad}}
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                CIF:
                                            </td>
                                            <td>
                                                {{$enterprise->cif}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Web:
                                            </td>
                                            <td>
                                                <a href="{{$enterprise->web}}" title="{{ $enterprise->user->name }}" target="_blank">Visitar Web</a>
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Pais:
                                            </td>
                                            <td>
                                                {{$enterprise->pais}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Ciudad:
                                            </td>
                                            <td>
                                                {{$enterprise->ciudad}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Empleados:
                                            </td>
                                            <td>
                                                {{$enterprise->min_empleados}} - {{$enterprise->max_empleados}}
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    {!! Form::close() !!}
                    @else
                    <article class="container">
                        <hr>
                        <h2 align="center">Vaya!</h2>
                        <h3 align="center">Parece que no hay nada que validar aquí.</h3>
                        <h4 align="center">Buen trabajo!</h4>
                        <hr>
                    </article>
                    @endif
                </div>


            </article>

        </div>
    </div>
</div> 