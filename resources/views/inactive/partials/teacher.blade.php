<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @if(!$teachers->isEmpty())
            {!! Form::open(['method' => 'post', 'route' => ['user.toggle.selected']]) !!}
            @endif
            <article class="container">
                @if(!$teachers->isEmpty())
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                            <input type="checkbox" class="check-padre" id="teacher">Marcar Todos
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-sm-offset-7">
                            <button class="btn btn-success" type="submit">Validar Seleccionados</button>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
                @endif
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                    @if(!$teachers->isEmpty())
                    @foreach($teachers as $teacher)
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <div class="checkbox">
                                  <h2><label><input type="checkbox" name="selected[]" id="teacher" value="{{$teacher->user->id}}">{{ $teacher->user->name }} <small>{{ $teacher->user->rol }}</small></label></h2>
                                </div>
                                <ul class="nav navbar-right panel_toolbox">
                                   <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                        <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                             <li><a href="#">Settings 1</a></li>
                                             <li><a href="#">Settings 2</a></li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>

                            <div class="x_content">
                                   <table class="table table-condensed ">
                                    <tr>
                                        <td>
                                            Nombre:
                                        </td>
                                        <td>
                                            {{$teacher->user->name}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Apellidos:
                                        </td>
                                        <td>
                                            {{$teacher->apellidos}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            NRP/Expediente:
                                        </td>
                                        <td>
                                            {{$teacher->nrp_expediente}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Telefono:
                                        </td>
                                        <td>
                                            {{$teacher->user->phone}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Email:
                                        </td>
                                        <td>
                                            {{$teacher->user->email}}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                @endforeach
                {!! Form::close() !!}
                @else
                <article class="container">
                    <hr>
                    <h2 align="center">Vaya!</h2>
                    <h3 align="center">Parece que no hay nada que validar aquí.</h3>
                    <h4 align="center">Buen trabajo!</h4>
                    <hr>
                </article>
                @endif

            </div>


        </article>
        <br>
        <br>
        <br>
    </div>
</div>
</div>